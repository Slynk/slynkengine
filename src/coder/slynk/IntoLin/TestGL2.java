package coder.slynk.IntoLin;

import javax.microedition.khronos.opengles.GL10;

import android.util.Log;

import coder.slynk.Engine2D.Position;
import coder.slynk.Engine2D.Proportions;
import coder.slynk.Engine2D.Util;
import coder.slynk.Engine2D.Graphics.AnimatedSprite;
import coder.slynk.Engine2D.Graphics.Image;
import coder.slynk.Engine2D.Graphics.Sprite;
import coder.slynk.Engine2D.Graphics.SpriteSplitter;
import coder.slynk.Engine2D.States.State;
import coder.slynk.Engine2D.States.StateManager;

public final class TestGL2 extends State {
	private AnimatedSprite image;
	private Sprite image2;

	
	private double X = 0.0d;
	private double dX = 5.0d;
	
	private float A = 1.0f;
	private float dA = -0.01f;
	
	@Override
	protected void _textures() {
		Proportions frameSize = new Proportions(128, 128);
		Image imgAnimation = _loadTexture(R.drawable.animation);
		Image imgAndroid = _loadTexture(R.drawable.android);
		
		SpriteSplitter splitAnimation = new SpriteSplitter(frameSize, imgAnimation);
		
		image = new AnimatedSprite(
				
				new Position(200.0f, 200.0f),                           // Initial Position of Sprite
				splitAnimation,         								// Sprite Sheet Associated with Sprite
				new int[][]{{15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0}},   // An animation array. It's an array of animation arrays. This is the order the frames appear.
				15,                                                     // The frames to be displayed per second
				new Proportions(256, 256)                         		// The width/height of the Sprite
				
		);
		image2 = new Sprite(imgAndroid, new Position(0.0f, 100.0f));
	}
	
	@Override
	protected void _resources() {
		
	}
	
	@Override
	protected void _input(double x, double y) {
		image2.moveTo((float)x, (float)y);
		X = x;
	}

	@Override
	protected void _update(final long deltaNs) {
		int res = Util.checkRange(0.0f, 1100.0f, X);
		
		switch(res)
		{
		case -1:
			X = 0.0f;
			dX = 5.0f;
			break;
		case 1:
			_shouldChangeState = true;
			_switchStateNum = 0;
			X = 1100.0f;
			dX = -5.0f;
			break;
		default:
			break;
		}
		X += dX;
		
		res = Util.checkRange(0.0f, 1.0f, A);
		
		switch(res)
		{
		case -1:
			A = 0.0f;
			dA = 0.01f;
			break;
		case 1:
			A = 1.0f;
			dA = -0.01f;
			break;
		default:
			break;
		}
		A += dA;
			
		image2.moveBy((float)dX, 0.0f);
		image2.fade(A);
	}

	@Override
	protected void _draw(GL10 gl) {
		image.draw(gl);
		image2.draw(gl);
	}

	@Override
	protected void _cleanup(){}

	@Override
	public void _pause() {
	}

	@Override
	public void _resume() {
	}

	@Override
	protected void _changeState(StateManager manager) {
		switch(_switchStateNum)
		{
			case 0:
				manager.popState();
				break;
			case 1:
				break;
			default:
				Log.v("IntoLin", "TestGL.ChangeState() failed. switchStateNum = " + _switchStateNum);
				break;
		}
		
	}

	
}
