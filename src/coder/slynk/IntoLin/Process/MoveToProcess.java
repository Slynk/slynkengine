package coder.slynk.IntoLin.Process;

import coder.slynk.Engine2D.Position;
import coder.slynk.Engine2D.Glue.Time;
import coder.slynk.Engine2D.Graphics.AnimatedSprite;
import coder.slynk.Engine2D.Process.Process;

public class MoveToProcess extends Process {
	private final Position m_To;
	private final double m_DistancePerSecond;
	private final AnimatedSprite m_Player;
	
	public MoveToProcess(final AnimatedSprite player, final double distancePerSecond, final Position to){
		m_Player = player;
		m_DistancePerSecond = distancePerSecond;
		m_To = to;
	}

	@Override
	protected void vOnUpdate(long deltaNs) {
		final Position a = m_Player.getPosition();
		
		if(!(a.x == m_To.x && a.y == m_To.y)){
			// a. calculate the vector from o to g:
            final double vectorX = m_To.x - a.x;
            final double vectorY = m_To.y - a.y;
            // b. calculate the length:
            final double magnitude = Math.sqrt(vectorX * vectorX + vectorY * vectorY);

            // d. calculate and Draw the new vector, which is x1y1 + vxvy * (mag + distance).
            final double percent = (m_DistancePerSecond * (deltaNs / (double)Time.SECONDS)) / magnitude;
            final Position c = new Position(
            		a.x + ((m_To.x - a.x) * percent),
            		a.y + ((m_To.y - a.y) * percent)
            );
            
	        if(percent >= 1d){
	        	m_Player.moveTo((float)m_To.x, (float)m_To.y);
	        	succeed();
	        } else
	        	m_Player.moveTo((float)c.x, (float)c.y);
		}
		else
			succeed();
	}

}
