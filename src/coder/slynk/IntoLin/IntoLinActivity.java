package coder.slynk.IntoLin;

import coder.slynk.Engine2D.GameThread;
import coder.slynk.Engine2D.Process.ProcessManager;
import android.app.Activity;
import android.os.Bundle;

public class IntoLinActivity extends Activity {	
	GameThread game;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		game = new GameThread(new TestGL(new ProcessManager()), this);
		game.start();
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		game.pause();
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		game.unpause();
	}
	
	@Override
	protected void onStop(){
		game.destroy();
		super.onStop();
	}

	@Override
	protected void onDestroy(){
		super.onDestroy();
	}
}
