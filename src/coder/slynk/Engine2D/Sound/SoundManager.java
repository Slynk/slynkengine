package coder.slynk.Engine2D.Sound;

import java.util.HashMap;

import coder.slynk.Engine2D.Util;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.Log;

/**Manages background music and sound effects.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public class SoundManager {
	private final Context _context;
	private MediaPlayer _bgmPlayer;
	
	private final SoundPool _soundPool;
	private final HashMap<Integer, Integer> soundPoolMap;
	
	private int sfxCount;
	private final int[] bgmArray;
	
	/**Instantiates SoundManager with two arrays of resource IDs.
	 * 
	 * @param context context of the activity.
	 * @param sfxArray an array of sound effects resource IDs.
	 * @param bgmArray an array of background music resource IDs.
	 */
	public SoundManager(Context context, int[] sfxArray, int[] bgmArray)
	{
		_context = context;
		_bgmPlayer = new MediaPlayer();
		
		_soundPool = new SoundPool(sfxArray.length+1, AudioManager.STREAM_MUSIC, 0);
		soundPoolMap = new HashMap<Integer, Integer>(sfxArray.length+bgmArray.length);
		
		sfxCount = sfxArray.length;
		this.bgmArray = bgmArray;
		
		for(int i = 0; i < sfxCount; i++)
		{
			soundPoolMap.put(i, _soundPool.load(context, sfxArray[i], 1));
		}
	}
	
	/**Fades the current bgm to the given bgm number.
	 * 
	 * @param to the index of the bgm to fade to.
	 */
	public void fadeTo(int to)
	{
		if(Util.checkRange(0, bgmArray.length - 1, to) == 0)
		{
			_bgmPlayer.release();
			_bgmPlayer = MediaPlayer.create(_context, bgmArray[to]);
			_bgmPlayer.setLooping(true);
			_bgmPlayer.start();
		}
		else
			Log.v("SlynkEngine", "SoundManager.fadeTo() was out of range");
	}
	
	/**Fires off a sound effect.
	 * 
	 * @param x the index of the sfx to fire.
	 */
	public void fire(int x)
	{		
		if(Util.checkRange(0, sfxCount, x) == 0)
			_soundPool.play(soundPoolMap.get(x), 1.0f, 1.0f, 1, 0, 1f);
		else
			Log.v("SlynkEngine", "SoundManager.fire() was out of range");
		
		
	}
	
	/**Add to state.Pause();*/
	public void pause()
	{
		_bgmPlayer.pause();
	}
	
	/**Add to state.Resume()*/
	public void resume()
	{
		_bgmPlayer.start();
	}
	
	/**Add to state.Cleanup*/
	public void cleanup()
	{
		_bgmPlayer.release();
		_soundPool.release();
	}
}
