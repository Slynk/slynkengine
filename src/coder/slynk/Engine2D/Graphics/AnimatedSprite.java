package coder.slynk.Engine2D.Graphics;

import javax.microedition.khronos.opengles.GL10;

import coder.slynk.Engine2D.Proportions;
import coder.slynk.Engine2D.Position;

/**A sprite object consisting of multiple frames in a single texture.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public class AnimatedSprite extends Square{
	private int[][] _animations;
	private int _FPS;
	private int _count = 0;
	private int _currentAnimation = 0;
	private int _currentFrame = 0;
	private SpriteSplitter _ss;
	private boolean _paused = true;
	
	/**Creates an animated sprite.
	 * 
	 * @param pos starting screen position.
	 * @param sheet SpriteSplitter object containing the texture and frames for the sprite.
	 * @param animationsArray array animation arrays.
	 * @param fps speed at which the animations occur.
	 * @param size the size of the sprite object.
	 */
	public AnimatedSprite(Position pos, SpriteSplitter sheet, int[][] animationsArray, int fps, Proportions size) {
		super(pos, size);
		_FPS = fps;
		_ss = sheet;
		_animations = animationsArray;
		_color = new Color4f(1.0f, 1.0f, 1.0f, 1.0f);
	}
	
	/**Change the animation speed.
	 * 
	 * @param speed new speed.
	 */
	public void changeSpeed(int speed)
	{
		_FPS = speed;
	}
	
	/**Draw the object.
	 *
	 * @param gl GL10 reference.
	 */
	public void draw(GL10 gl)
	{
		gl.glColor4f(_color.r, _color.g, _color.b, _color.a);
		_ss.drawFrame(gl, _animations[_currentAnimation][_currentFrame], _vertexBuffer);
		
		if(!_paused)
		{
			if(_count >= (60/_FPS))
			{
				_currentFrame++;
				_count=0;
			}
			else
				_count++;
			
			if(_currentFrame >= _animations[_currentAnimation].length)
				_currentFrame = 0;
		}
	}
	
	/**Changes the animation array to cycle through.
	 * 
	 * @param which the index of the new array to cycle through.
	 */
	public void setAnimation(int which)
	{
		_currentAnimation = which;
		_currentFrame = 0;
		_paused = false;
	}
	
	/**Pauses the animation cycle.*/
	public void stop()
	{
		_paused = true;
	}
	
	/**Pauses the animation cycle at a given frame in the current animation cycle.
	 * 
	 * @param which the frame to pause on. 
	 */
	public void stop(int which)
	{
		_currentFrame = which;
		_paused = true;
	}
	/**Resumes the animation cycle.*/
	public void play()
	{
		_paused = false;
	}
	
	/**Resumes the animation cycle at a given frame in the current animation cycle.
	 * 
	 * @param which the frame to resume on. 
	 */
	public void play(int which)
	{
		_currentFrame = which;
		_paused = false;
	}

}
