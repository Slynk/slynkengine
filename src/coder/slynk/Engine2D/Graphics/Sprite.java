package coder.slynk.Engine2D.Graphics;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import coder.slynk.Engine2D.Position;

/**Non-animated sprite to be drawn at a position.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public class Sprite extends Square {
	/**The image representing this sprite.*/
	protected Image _image;
	/**Buffer to store texture coordinates to be bound by OpenGL.*/
	protected FloatBuffer _textureBuffer;

	/**Instantiates a Sprite object at (0,0).
	 * 
	 * @param img the image representing the sprite.
	 */
	public Sprite(Image img) {
		super(new Position(), img.getSize());
		_image = img;
		
		_color = new Color4f(1.0f, 1.0f, 1.0f, 1.0f);
		
		ByteBuffer vbb = ByteBuffer.allocateDirect(32);
		vbb.order(ByteOrder.nativeOrder());
		_textureBuffer = vbb.asFloatBuffer();
		_textureBuffer.put(_image.getTextureCoordinates());
		_textureBuffer.position(0);
	}
	
	/**Instantiates a Sprite object at a given position.
	 * 
	 * @param img the image representing the sprite.
	 * @param pos the starting position of the top left corner of the sprite.
	 */
	public Sprite(Image img, Position pos) {
		super(pos, img.getSize());
		_image = img;
		
		_color = new Color4f(1.0f, 1.0f, 1.0f, 1.0f);
		
		ByteBuffer vbb = ByteBuffer.allocateDirect(32);
		vbb.order(ByteOrder.nativeOrder());
		_textureBuffer = vbb.asFloatBuffer();
		_textureBuffer.put(_image.getTextureCoordinates());
		_textureBuffer.position(0);
	}
	
	/**Gets X position.*/
	public double x()
	{
		return _position.x;
	}
	
	/**Gets Y position.*/
	public double y()
	{
		return _position.y;
	}
	
	/**Gets the width.*/
	public float width()
	{
		return this._size.width;
	}
	
	/**Gets the height.*/
	public float height()
	{
		return this._size.height;
	}
	
	/**Draws the sprite.*/
	@Override
	public void draw(GL10 gl)
	{
		gl.glColor4f(_color.r, _color.g, _color.b, _color.a);
		gl.glBindTexture(GL10.GL_TEXTURE_2D, _image.retrieveID());
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		
		gl.glDepthMask(false);
		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);

		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, _vertexBuffer);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, _textureBuffer);

		
		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
		
		gl.glDepthMask(true);
		gl.glDisable(GL10.GL_BLEND);
		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

	}
}