package coder.slynk.Engine2D.Graphics;

import java.util.concurrent.ConcurrentLinkedQueue;

import android.opengl.GLES20;

/**
 * Facade for the graphical subsystem.
 * 
 * @author Slynk
 *
 */
public final class Graphics {
	/**
	 * Variables
	 */
	private static ConcurrentLinkedQueue<RunnableGL> toDraw = new ConcurrentLinkedQueue<RunnableGL>();
	
	/**
	 * Private methods
	 */
	
	private static final class GraphicsHolder {
		static final Graphics singleton = new Graphics();
	}

	private Graphics() {}
	
	/**
	 * Public Interface
	 */

	public static Graphics getInstance() {
		return GraphicsHolder.singleton;
	}
	
	public static void draw(final RunnableGL run){
		toDraw.add(run);
	}
	
	public static void drawAll(final GLES20 gl){
		RunnableGL run = toDraw.peek();
		while(run != null){
			run.run(gl);
			run = toDraw.peek();
		}
	}
}
