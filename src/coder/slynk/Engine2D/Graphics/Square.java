package coder.slynk.Engine2D.Graphics;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import coder.slynk.Engine2D.Position;
import coder.slynk.Engine2D.Proportions;

/**A basic colored square.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public class Square {
	/**The draw color associated with the square.*/
	protected Color4f _color;
	/**The (x,Y) position of the square's top left corner.*/
	protected Position _position;
	/** the height and width of the square.*/
	protected Proportions _size;

	/**The vertex buffer to be bound by OpenGL.*/
	protected FloatBuffer _vertexBuffer;
	
	/**Instantiate Square.
	 * 
	 * @param X the x position of the top left point of the square.
	 * @param Y the y position of the top left point of the square.
	 * @param dim the size of the square.
	 */
	public Square(Position pos, Proportions dim){
		
		_position = pos;
		_size = dim;
		
		_color = new Color4f(1.0f, 1.0f, 0.0f, 1.0f);
		
		float[] vertices = new float[] {
				(float) _position.x, (float) _position.y, 0.0f,    // top left
				(float) _position.x, (float) (_position.y +_size.height), 0.0f,  // bottom left
				(float) (_position.x+_size.width), (float) _position.y, 0.0f,// top right
				(float) (_position.x+_size.width), (float) (_position.y+_size.height), 0.0f   // bottom right
		};
		
		ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
		vbb.order(ByteOrder.nativeOrder());
		_vertexBuffer = vbb.asFloatBuffer();
		_vertexBuffer.put(vertices);
		_vertexBuffer.position(0);
	}
	
	/**Returns the vertex buffer.*/
	public FloatBuffer getBuffer(){return _vertexBuffer;}
	
	/**Scales the square by the ratio provided.
	 * 
	 * @param amount the decimal percentage to scale by for both width and height. Ex: amount=0.5, size is halved. 
	 */
	public void scale(double amount)
	{		
		_size.width *= amount;
		_size.height *= amount;
		
		//Bottom Left
		_vertexBuffer.put(4, (float) (_position.y+_size.height));
		
		//Top Right
		_vertexBuffer.put(6, (float) (_position.x+_size.width));
		
		//Bottom Right
		_vertexBuffer.put(9, (float) (_position.x+_size.width));
		_vertexBuffer.put(10, (float) (_position.y+_size.height));
		_vertexBuffer.position(0);
	}
	
	/**Scales the square by the ratio provided.
	 * 
	 * @param widthScale amount the decimal percentage to scale by for width. Ex: amount=0.1, width is reduced to 1/10 size. 
	 * @param heightScale amount the decimal percentage to scale by for height. Ex: amount=1.0, height is unchanged. 
	 */
	public void scale(float widthScale, float heightScale)
	{
		_size.width *= widthScale;
		_size.height *= heightScale;
		
		//Bottom Left
		_vertexBuffer.put(4, (float) (_position.y+_size.height));
		
		//Top Right
		_vertexBuffer.put(6, (float) (_position.x+_size.width));
		
		//Bottom Right
		_vertexBuffer.put(9, (float) (_position.x+_size.width));
		_vertexBuffer.put(10, (float) (_position.y+_size.height));
		
		_vertexBuffer.position(0);
	}
	
	/**Moves square to location.
	 * 
	 * @param X new X position.
	 * @param Y new Y position.
	 */
	public void moveTo(float X, float Y){
		//Top Left		
		_vertexBuffer.put(0, X);
		_vertexBuffer.put(1, Y);
		
		//Bottom Left
		_vertexBuffer.put(3, X);
		_vertexBuffer.put(4, Y+_size.height);
		
		//Top Right
		_vertexBuffer.put(6, X+_size.width);
		_vertexBuffer.put(7, Y);
		
		//Bottom Right
		_vertexBuffer.put(9, X+_size.width);
		_vertexBuffer.put(10, Y+_size.height);
		
		_vertexBuffer.position(0);
		
		_position.x = X;
		_position.y = Y;
	}
	
	/**Shifts the square by a given amount.
	 * 
	 * @param X shift X pixels to the right. (negative, to the left)
	 * @param Y shift Y pixels down.(negative, up)
	 */
	public void moveBy(float X, float Y){	
		//Top Left		
		_vertexBuffer.put(0, _vertexBuffer.get(0)+X);
		_vertexBuffer.put(1, _vertexBuffer.get(1)+Y);
		
		//Bottom Left
		_vertexBuffer.put(3, _vertexBuffer.get(3)+X);
		_vertexBuffer.put(4, _vertexBuffer.get(4)+Y);
		
		//Top Right
		_vertexBuffer.put(6, _vertexBuffer.get(6)+X);
		_vertexBuffer.put(7, _vertexBuffer.get(7)+Y);
		
		//Bottom Right
		_vertexBuffer.put(9, _vertexBuffer.get(9)+X);
		_vertexBuffer.put(10, _vertexBuffer.get(10)+Y);
		
		_vertexBuffer.position(0);
		
		_position.x += X;
		_position.y += Y;
	}
	
	/**Sets a new size for the square.
	 * 
	 * @param width new width.
	 * @param height new height.
	 */
	public void resize(int width, int height){
		//Bottom Left
		_vertexBuffer.put(4, _vertexBuffer.get(4) - (_size.height-height));
		
		//Top Right
		_vertexBuffer.put(6, _vertexBuffer.get(6) - (_size.width-width));
		
		//Bottom Right
		_vertexBuffer.put(9, _vertexBuffer.get(9) - (_size.width-width));
		_vertexBuffer.put(10, _vertexBuffer.get(10) - (_size.height-height));
		
		_size.width = width;
		_size.height = height;
		
		_vertexBuffer.position(0);
	}
	
	/**Sets a new alpha for the square.
	 * 
	 * @param opacity new alpha.
	 */
	public void fade(float opacity)
	{
		
		_color.setColor(_color.r, _color.g, _color.b, opacity);
	}
	
	/**Sets a new color for the square.
	 * 
	 * @param c new color.
	 */
	public void setColor(Color4f c)
	{
		_color = c;
	}
	
	/**Sets a new color for the square.
	 * 
	 * @param R Red.
	 * @param G Green.
	 * @param B Blue.
	 * @param A Alpha.
	 */
	public void setColor(float R, float G, float B, float A)
	{
		_color = new Color4f(R, G, B, A);
	}

	/**Draw the square.
	 * 
	 * @param gl GL10 reference.
	 */
	public void draw(GL10 gl) {

		gl.glColor4f(_color.r, _color.g, _color.b, _color.a);
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		
		gl.glDepthMask(false);
		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, _vertexBuffer);
		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
		
		gl.glDepthMask(true);
		gl.glDisable(GL10.GL_BLEND);
		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY); 
	}
	
	public Position getPosition(){
		return _position;
	}
}