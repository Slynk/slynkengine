package coder.slynk.Engine2D.Graphics;

import android.opengl.GLES20;

public interface RunnableGL {
	public void run(GLES20 gl);
}
