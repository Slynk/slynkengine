package coder.slynk.Engine2D.Graphics;

import coder.slynk.Engine2D.Proportions;

/** Represents a loaded image (potentially cropped.)
 * 
 * @author Slynk
 *
 */
public class Image {
	private int _textureID;
	private final int _resourceID;
	
	private final Proportions _size;
	private final ImageCrop _crop;
	
	private final float[] _textureCoordinates;
	
	/** Use the full image without cropping.
	 * 
	 * @param resource the resource ID.
	 * @param ID the texture ID.
	 * @param dimensions the width and height of the full image.
	 */
	public Image(int resource, int ID, Proportions dimensions)
	{
		short zero = 0;
		_textureID = ID;
		_resourceID = resource;
		_size = dimensions;
		_crop = new ImageCrop(zero, zero, zero, zero);
		_textureCoordinates = new float[]{
				0.0f, 1.0f, // TL
				0.0f, 0.0f, // BL 
				1.0f, 1.0f, // TR
				1.0f, 0.0f  // BR
		};
	}
	
	/** Use the image with cropping.
	 * 
	 * @param resource the resource ID.
	 * @param ID the texture ID.
	 * @param dimensions the width and height of the full image.
	 * @param crop the cropping area of the image.
	 */
	public Image(int resource, int ID, Proportions dimensions, ImageCrop crop)
	{
		_textureID = ID;
		_resourceID = resource;
		_size = dimensions;
		_crop = crop;
		
		_textureCoordinates = new float[]{
				(float)_crop.left/_size.width, 1f - ((float)_crop.top/_size.height), // TL
				(float)_crop.left/_size.width, (float)_crop.bottom/_size.height, // BL 
				1f - ((float)_crop.right/_size.width), 1f - ((float)_crop.top/_size.height), // TR
				1f - ((float)_crop.right/_size.width), (float)_crop.bottom/_size.height  // BR
		};
	}
	
	/**Returns the texture ID.*/
	public int retrieveID()
	{
		return _textureID;
	}
	
	/**Returns the Resource ID.*/
	public int retrieveResourceID()
	{
		return _resourceID;
	}
	
	/**Returns the texture coordinates of the image with/without cropping.*/
	public  float[] getTextureCoordinates()
	{
		
		return _textureCoordinates;
	}
	
	/**Returns the proportions of the full image*/
	public Proportions getFullSize()
	{
		return _size;
	}
	
	/**Returns the proportions of the image with cropping**/
	public Proportions getSize()
	{
		return new Proportions(_size.width - _crop.left - _crop.right, _size.height - _crop.top - _crop.bottom);
	}
	
	/**Returns the full image width without cropping*/
	public int fullWidth()
	{
		return _size.width;
	}
	
	/**Returns the image width with cropping*/
	public int width()
	{
		return (_size.width - _crop.left - _crop.right);
	}
	
	/**Returns the full image height without cropping*/
	public int fullHeight()
	{
		return _size.height;
	}
	
	/**Returns the image height with cropping*/
	public int height()
	{
		return (_size.height - _crop.top - _crop.bottom);
	}
	
	public ImageCrop getCrop()
	{
		return _crop;
	}
	
	public short getLeftCrop()
	{
		return _crop.left;
	}
	
	public short getRightCrop()
	{
		return _crop.right;
	}
	
	public short getTopCrop()
	{
		return _crop.top;
	}
	
	public short getBottomCrop()
	{
		return _crop.bottom;
	}
	
	/**Used to set the texture ID after reloading the texture when OpenGL deletes its context.
	 * This happens when the application is minimized.
	 * 
	 * @param ID
	 */
	public void setNewTextureID(int ID)
	{
		_textureID = ID;
	}
}
