package coder.slynk.Engine2D.Graphics;

/** Holds pixel offset information for a cropped image.
 * 
 * @author Slynk < coder.slynk @ gmail.com >
 * @version v1.0 Jan 1, 2012
 */
public class ImageCrop {
	/** Holds offset. */
	public final short left, right, top, bottom;
	
	/**
	*
	* @param l the pixels from the left to crop.
	* @param r the pixels from the right to crop.
	* @param t the pixels from the top to crop.
	* @param b the pixels from the bottom to crop.
	*/
	public ImageCrop(short l, short r, short t, short b)
	{
		left = l;
		right = r;
		top = t;
		bottom = b;
	}
}
