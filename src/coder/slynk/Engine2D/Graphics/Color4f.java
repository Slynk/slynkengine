package coder.slynk.Engine2D.Graphics;

/**Houses RGBA color information.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public class Color4f {
	public float r, g, b, a;
	
	/**Create Color
	 * 
	 * @param rr Red
	 * @param gg Green
	 * @param bb Blue
	 * @param aa Alpha
	 */
	public Color4f(float rr, float gg, float bb, float aa)
	{
		r = rr;
		g = gg;
		b = bb;
		a = aa;
	}
	
	/**Sets the color.
	 * 
	 * @param rr Red
	 * @param gg Green
	 * @param bb Blue
	 * @param aa Alpha
	 */
	public void setColor(float rr, float gg, float bb, float aa)
	{
		r = rr;
		g = gg;
		b = bb;
		a = aa;
	}

}
