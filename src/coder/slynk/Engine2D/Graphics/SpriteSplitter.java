package coder.slynk.Engine2D.Graphics;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import android.util.Log;

import coder.slynk.Engine2D.Proportions;

/**Capable of splitting the currently held image into sub images.
 * Useful for sprite sheets and tile sheets.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public class SpriteSplitter {
	/**The image associated with this sheet*/
	protected Image _image;
	/**Texture coordinate buffer to be bound by OpenGL.*/
	protected FloatBuffer _textureBuffer;
	
	/**Dimensions of a single frame.*/
	protected Proportions _frameDim;
	/**Border width and height between frames.*/
	protected Proportions _border;
	
	/**Combination of frame width and border width converted into a texture coordinate.
	 * (By dividing the width by the texture width.)*/
	protected float _stepW;
	/**Combination of frame height and border height converted into a texture coordinate.
	 * (By dividing the height by the texture width.)*/
	protected float _stepH;
	/**Number of frames in a row.*/
	protected int _framesWide;
	/**Number of frames in a column.*/
	protected int _framesTall;
	/**Total number of frames.*/
	protected int _frameCount;
	
	/**Reading left to right, top to bottom, this is the current frame that is being used.*/
	protected int _currentFrame = -1;
	
	/**Sheet with no border between frames.
	 * 
	 * @param frameD width and height of a frame within the texture.
	 * @param img the image associated with the sheet.
	 */
	public SpriteSplitter(Proportions frameD, Image img) {
		_frameDim = frameD;
		_border = new Proportions(0, 0);
		_image = img;
		
		ByteBuffer vbb = ByteBuffer.allocateDirect(32);
		vbb.order(ByteOrder.nativeOrder());
		_textureBuffer = vbb.asFloatBuffer();
		
		_stepW = ((float)(_frameDim.width + _border.width)/_image.fullWidth());
		_stepH = ((float)(_frameDim.height + _border.height)/_image.fullHeight());
		_framesWide = (int)((_image.width() - _border.width)/(_frameDim.width + _border.width));
		_framesTall = (int)((_image.height() - _border.height)/(_frameDim.height + _border.height));
		_frameCount = _framesWide*_framesTall;
	}
	
	/**Sheet with border between frames.
	 * 
	 * @param frameD width and height of a frame within the texture.
	 * @param BORDER the border between frames.
	 * @param img the image associated with the sheet.
	 */
	public SpriteSplitter(Proportions frameD, Proportions BORDER, Image img) {
		_frameDim = frameD;
		_border = BORDER;
		_image = img;
		
		ByteBuffer vbb = ByteBuffer.allocateDirect(32);
		vbb.order(ByteOrder.nativeOrder());
		_textureBuffer = vbb.asFloatBuffer();
		
		_stepW = ((float)(_frameDim.width + _border.width)/_image.fullWidth());
		_stepH = ((float)(_frameDim.height + _border.height)/_image.fullHeight());
		_framesWide = (int)((_image.width() - _border.width)/(_frameDim.width + _border.width));
		_framesTall = (int)((_image.height() - _border.height)/(_frameDim.height + _border.height));
		_frameCount = _framesWide*_framesTall;
	}
	
	/**Draws a frame from the sheet with a given vertex buffer. 
	 * 
	 * @param gl GL10 reference.
	 * @param frame the frame index to draw.
	 * @param vertexBuffer the buffer housing four vertex points to house the texture.
	 */
	public void drawFrame(GL10 gl, int frame, FloatBuffer vertexBuffer)
	{
		_calculateTexturePoints(frame);

		gl.glBindTexture(GL10.GL_TEXTURE_2D, _image.retrieveID());
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		
		gl.glDepthMask(false);
		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);

		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, _textureBuffer);

		
		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
		
		gl.glDepthMask(true);
		gl.glDisable(GL10.GL_BLEND);
		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
	}

	/**Calculates and stores the given frame's texture coordinates in the buffer.
	 * 
	 * Make sure to check if the current frame is the same as the new frame before calling this. EX:
	 *  
	 * if(frame != currentFrame){ calculateTexturePoints(frame); }
	 * 
	 * @param frame the frame to calculate.
	 */
	protected void _calculateTexturePoints(int frame)
	{
		if(frame != _currentFrame)
		{
			_textureBuffer.position(0);
			_textureBuffer.put(getTextureCoordinates(frame));
			_textureBuffer.position(0);
		}
	}
	
	public Proportions getFrameDimensions()
	{
		return _frameDim;
	}
	
	public int getFrameWidth()
	{
		return _frameDim.width;
	}
	
	public int getFrameHeight()
	{
		return _frameDim.height;
	}
	
	public int retrieveID()
	{
		return _image.retrieveID();
	}
	
	public float[] getTextureCoordinates(int frame)
	{
		if((frame < 0) || (frame >=_frameCount))
			Log.v("SlynkEngine", "SpriteSheet.drawFrame("+ frame +") out of bounds. Max is " + _frameCount);
		else
		{
			float xx, yy;
			
			if(frame !=0)
			{
				xx = ((frame % _framesWide) * _stepW) + (float)_border.width/_image.fullWidth() + (float)_image.getLeftCrop()/_image.fullWidth();
				yy = (1.0f - ((frame / _framesWide) * _stepH)) - (float)_border.height/_image.fullHeight() - (float)_image.getTopCrop()/_image.fullHeight();
			}
			else
			{
				xx = 0.0f + (float)_border.width/_image.fullWidth() + (float)_image.getLeftCrop()/_image.fullWidth();
				yy = 1.0f - (float)_border.height/_image.fullHeight() - (float)_image.getTopCrop()/_image.fullHeight();
			}
			
			_currentFrame = frame;
			return new float[]{
					xx, yy,
					xx, yy-(float)_frameDim.height/_image.fullHeight(),
					xx+(float)_frameDim.width/_image.fullWidth(), yy, 
					xx+(float)_frameDim.width/_image.fullWidth(), yy-(float)_frameDim.height/_image.fullHeight()};
		}
		return null;
	}
}
