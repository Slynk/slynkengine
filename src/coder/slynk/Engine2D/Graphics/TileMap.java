package coder.slynk.Engine2D.Graphics;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import coder.slynk.Engine2D.DeviceInformation;
import coder.slynk.Engine2D.Proportions;

/**Takes an 3D array and texture to draw a multiple layered tile map.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public class TileMap {
	private SpriteSplitter _ss;
	
	private int _indexID;
	private int _textureID;
	private int _vertexID;
	
	private int _verticesCount;
	
	private ShortBuffer _indexBuffer;
	private FloatBuffer _vertexBuffer;
	private FloatBuffer _textureCoordBuffer;
	
	private int _tilesWide, _tilesTall;
	private int _screenTilesW, _screenTilesH;
	private int _currentTileX, _currentTileY;
	private int _toDrawCount;
	private int _frameCount;
	
	private boolean _initialized = false;
	
	/**Creates a tile map with an existing SpriteSplitter.
	 * 
	 * @param sp existing sprite splitter.
	 * @param MAP 3D array of tile index. MAP[layer][x][y]
	 */
	public TileMap(SpriteSplitter sp, int[][][] MAP) {
		
		_ss = sp;
		
		_frameCount = MAP[0].length*MAP[0][0].length;
		initBuffers(MAP);
	}
	
	/**Creates a tile map with no borders between frames.
	 * 
	 * @param frameD tile dimensions.
	 * @param img the image associated with the sheet.
	 * @param MAP 3D array of tile index. MAP[layer][x][y]
	 */
	public TileMap(Proportions frameD, Image img, int[][][] MAP) {
		
		_ss = new SpriteSplitter(frameD, img);
		
		_frameCount = MAP[0].length*MAP[0][0].length;
		initBuffers(MAP);
	}
	
	/**Creates a tile map with borders between frames.
	 * 
	 * @param frameD tile dimensions.
	 * @param BORDER border between tiles.
	 * @param img the image associated with the sheet.
	 * @param MAP 3D array of tile index. MAP[layer][x][y]
	 */
	public TileMap(Proportions frameD, Proportions BORDER, Image img, int[][][] MAP) {
		
		_ss = new SpriteSplitter(frameD, BORDER, img);
		
		_frameCount = MAP[0].length*MAP[0][0].length;
		initBuffers(MAP);
	}
	
	private void initBuffers(int[][][] map)
	{
		_tilesTall = map[0].length;
		_tilesWide = map[0][0].length;
		
		_screenTilesW = (DeviceInformation.getWidth()/_ss.getFrameWidth()) + 1;
		_screenTilesH = (DeviceInformation.getHeight()/_ss.getFrameHeight()) + 1;
		
        ByteBuffer vbb = ByteBuffer.allocateDirect(_frameCount * 48);
        vbb.order(ByteOrder.nativeOrder());
        _vertexBuffer = vbb.asFloatBuffer();
        
        ByteBuffer tbb = ByteBuffer.allocateDirect(_frameCount * 32);
        tbb.order(ByteOrder.nativeOrder());
        _textureCoordBuffer = tbb.asFloatBuffer();
        
        int c;
        int lastFrame = -2;
        float[] working = new float[]{0,0,0,0,0,0,0,0};
        for(int j = 0; j < _tilesTall; j++)
		{
        	c = 0;
			for(int i : map[0][j])
			{
				_vertexBuffer.put(new float[]{c*_ss.getFrameWidth(),     j*_ss.getFrameHeight(),    0.0f, //TL
											 c*_ss.getFrameWidth(), 	  (j+1)*_ss.getFrameHeight(), 0.0f, //BL
											 (c+1)*_ss.getFrameWidth(),  j*_ss.getFrameHeight(),    0.0f, //TR
											(c+1)*_ss.getFrameWidth(), (j+1)*_ss.getFrameHeight(), 0.0f}); //BR
				
				if(i != lastFrame)
				{
					working = _ss.getTextureCoordinates(i);
					lastFrame = i;
				}
				
				_textureCoordBuffer.put(working);
				c++;
			}
		}
		
        fillIndexBuffer();
		_vertexBuffer.position(0);
		_textureCoordBuffer.position(0);
	}
	
	private void fillIndexBuffer()
	{
		_currentTileX = DeviceInformation.getX()/_ss.getFrameWidth();
		_currentTileY = DeviceInformation.getY()/_ss.getFrameHeight();
		
		if(_currentTileX < 0)
			_currentTileX = 0;
		if(_currentTileY < 0)
			_currentTileY = 0;
		
        
        int toDrawX = _currentTileX + _screenTilesW;
        int toDrawY = _currentTileY + _screenTilesH;
        
        if(toDrawX > _tilesWide)
        	toDrawX = _tilesWide;
        if(toDrawY > _tilesWide)
        	toDrawY = _tilesTall;
        
        _toDrawCount = toDrawX * toDrawY;
        
        ByteBuffer ibb = ByteBuffer.allocateDirect(_toDrawCount * 12);
        ibb.order(ByteOrder.nativeOrder());
        _indexBuffer = ibb.asShortBuffer();

		for(int j = _currentTileY; j < toDrawY; j++)
		{
			for(int i = _currentTileX; i < toDrawX; i++)
			{
				_indexBuffer.put(new short[]{
						(short) ((j*_tilesWide*4) + (i*4)),  
						(short) ((j*_tilesWide*4)+ (i*4) + 1), 
						(short) ((j*_tilesWide*4)+ (i*4) + 2), 
						(short) ((j*_tilesWide*4)+ (i*4) + 1), 
						(short) ((j*_tilesWide*4)+ (i*4) + 2), 
						(short) ((j*_tilesWide*4)+ (i*4) + 3)});
			}
		}
		_indexBuffer.position(0);
		_initialized = false;
	}
	
	private void initMap(GL11 gl)
	{
		int[] buffer = new int[1];
		gl.glGenBuffers(1, buffer, 0);
		_indexID = buffer[0];
		gl.glBindBuffer(GL11.GL_ELEMENT_ARRAY_BUFFER, _indexID);
		gl.glBufferData(GL11.GL_ELEMENT_ARRAY_BUFFER, _toDrawCount * 12, _indexBuffer, GL11.GL_STATIC_DRAW);
		
		gl.glGenBuffers(1, buffer, 0);
		_vertexID = buffer[0];
		gl.glBindBuffer(GL11.GL_ARRAY_BUFFER, _vertexID);
		gl.glBufferData(GL11.GL_ARRAY_BUFFER, _frameCount * 48, _vertexBuffer, GL11.GL_STATIC_DRAW);
		
		gl.glGenBuffers(1, buffer, 0);
		_textureID = buffer[0];
		gl.glBindBuffer(GL11.GL_ARRAY_BUFFER, _textureID);
		gl.glBufferData(GL11.GL_ARRAY_BUFFER, _frameCount * 32, _textureCoordBuffer, GL11.GL_STATIC_DRAW);
		
		gl.glBindBuffer(GL11.GL_ELEMENT_ARRAY_BUFFER, 0);
		gl.glBindBuffer(GL11.GL_ARRAY_BUFFER, 0);
		
		_verticesCount = _toDrawCount * 6;
		
		_initialized = true;
	}
	
	/**Draws the map.
	 * 
	 * @param gl GL10 reference.
	 */
	public void draw(GL10 gl)
	{		
		GL11 gl11 = (GL11)gl;
		if(((int) (DeviceInformation.getX()/_ss.getFrameWidth()) != _currentTileX) || ((int) (DeviceInformation.getY()/_ss.getFrameHeight()) != _currentTileY))
			fillIndexBuffer();
		
		if(!_initialized)
			initMap(gl11);
		
		
		
		gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		
		gl.glBindTexture(GL10.GL_TEXTURE_2D, _ss.retrieveID());
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		
		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		
		
		gl11.glBindBuffer(GL11.GL_ARRAY_BUFFER, _vertexID);
		gl11.glVertexPointer(3, GL11.GL_FLOAT, 0, 0);
		gl11.glBindBuffer(GL11.GL_ARRAY_BUFFER, _textureID);
		gl11.glBindBuffer(GL11.GL_ELEMENT_ARRAY_BUFFER, _indexID);
		gl11.glTexCoordPointer(2, GL11.GL_FLOAT, 0, 0);

		gl11.glDrawElements(GL11.GL_TRIANGLES, _verticesCount, GL11.GL_UNSIGNED_SHORT, 0);
		
		gl11.glBindBuffer(GL11.GL_ELEMENT_ARRAY_BUFFER, 0);
		gl11.glBindBuffer(GL11.GL_ARRAY_BUFFER, 0);
		
		gl.glDisable(GL10.GL_BLEND);
		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
	}
}
