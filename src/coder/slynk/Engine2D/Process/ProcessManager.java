package coder.slynk.Engine2D.Process;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class ProcessManager {
	private ArrayList<Process> m_ProcessList = new ArrayList<Process>();
	
	public ProcessManager(){}
	public void close(){
		clearAllProcesses();
	}
	
	// TODO Public Interface
	public int updateProcesses(long deltaNs){
		
		short successCount = 0;
		short failCount = 0;
		
		final ArrayList<Process> toRemove = new ArrayList<Process>();
		for(Process p : m_ProcessList){
			if(p.getState() == State.UNINITIALIZED)
				p.vOnInit();
			
			if(p.getState() == State.RUNNING)
				p.vOnUpdate(deltaNs);
			
			if(p.isDead()){
				final State s = p.getState();
				switch(s){
				case SUCCEEDED:
				{
					p.vOnSuccess();
					Process child = p.removeChild();
					if(child != null)
						attachProcess(child);
					else
						++successCount;
					break;
				}
				
				case FAILED:
				{
					p.vOnFail();
					++failCount;
					break;
				}
				
				case ABORTED:
				{
					p.vOnAbort();
					++failCount;
					break;
				}
				
				default:
					break;
				}
				
				toRemove.add(p);
			}
		}
		
		for(Process p : toRemove){
			m_ProcessList.remove(p);
			p.close();
			p = null;
		}
		
		return ((successCount << 16) | failCount);
	}
	public WeakReference<Process> attachProcess(final Process process){
		m_ProcessList.add(process);
		return new WeakReference<Process>(process);
	}
	public void abortAllProcesses(final boolean immediate){}
	
	
	//TODO Getters
	public int getProcessCount(){
		return m_ProcessList.size();
	}
	
	private void clearAllProcesses(){}
}
