package coder.slynk.Engine2D.Process;

public abstract class Process {
	private State m_State = State.UNINITIALIZED;
	private Process m_Child = null;
	
	public Process(){};
	public void close(){}
	
	// TODO Interface for child class
	protected void vOnInit(){
		m_State = State.RUNNING;
	};
	protected abstract void vOnUpdate(final long deltaNs);
	protected void vOnSuccess(){}
	protected void vOnFail(){}
	protected void vOnAbort(){}
	
	
	// TODO Functions to end processes
	public void succeed(){
		setState(State.SUCCEEDED);
	}
	public void fail(){
		setState(State.FAILED);
	}
	public void pause(){
		setState(State.PAUSED);
	}
	public void unPause(){
		setState(State.RUNNING);
	}
	
	// TODO Getters
	 
	public final State getState(){
		return m_State;
	}
	
	public final boolean isAlive(){
		return (m_State == State.RUNNING || m_State == State.PAUSED);
	}
	
	public final boolean isDead(){
		return (m_State == State.SUCCEEDED || m_State == State.FAILED || m_State == State.ABORTED);
	}
	
	public final boolean isRemoved(){
		return m_State == State.REMOVED;
	}
	
	public final boolean isPaused(){
		return m_State == State.PAUSED;
	}
	
	// TODO Child Functions
	public void attachChild(final Process child){		
		if(m_Child == null)
			m_Child = child;
		else
			m_Child.attachChild(child);
	}
	public Process removeChild(){
		final Process ret = m_Child;
		m_Child = null;
		
		return ret;
	}
	public Process peekChild(){
		return m_Child;
	}
	
	private void setState(final State newState){
		m_State = newState;
	}
}
