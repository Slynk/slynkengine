package coder.slynk.Engine2D.Process;

public enum State {
	UNINITIALIZED,
	REMOVED,
	RUNNING,
	PAUSED,
	SUCCEEDED,
	FAILED,
	ABORTED
}
