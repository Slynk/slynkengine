package coder.slynk.Engine2D.Process;

public class DelayProcess extends Process {
	private long m_TimeToDelay;
	private long m_TimeDelayedSoFar;
	
	public DelayProcess(final long timeToDelay){
		m_TimeToDelay = timeToDelay;
		m_TimeDelayedSoFar = 0;
	}

	@Override
	public void close() {
	}

	@Override
	protected void vOnUpdate(long deltaNs) {
		m_TimeDelayedSoFar += deltaNs;
		if(m_TimeDelayedSoFar >= m_TimeToDelay)
			succeed();
	}

}
