package coder.slynk.Engine2D;

import android.app.Activity;
import android.util.Log;

import coder.slynk.Engine2D.States.StateManager;

/** GameThread houses the game loop for the engine.
 * 
 * The thread is fixed at 60 frames per seconds and runs on its own thread.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public class GameThread extends Thread {
	private final int _FPS = 60;
	private final long _STEP = 1000 / _FPS;
	
	private StateManager _manager;
	
	private boolean _running = true;
	private boolean _paused = false;
	
	/**Instantiates GameThread and sets the first state.
	 * 
	 * @param firstState the fist state to be pushed to the state manager.
	 * @param main used to store a reference to the activity.
	 */
	public GameThread(coder.slynk.Engine2D.States.State firstState, Activity main){
		_manager = new StateManager(firstState, main);
	}
	
	/**Call inside Activity's onPause() method.*/
	public void pause(){
		Log.v("Slynk", "game.pause()"); 
		_paused = true; 
		_manager.pause();
	}
	
	
	/**Call inside Activity's onResume() method.*/
	public void unpause(){
		Log.v("Slynk", "game.unpause()"); 
		_paused = false; 
		_manager.resume();
	}
	
	
	/**Call inside Activity's onDestroy() method.*/
	public void destroy(){ 
		Log.v("Slynk", "game.destroy()");
		_running = false;
	}

	/**DO NOT USE! Call start() instead.*/
	@Deprecated
	public void run() {
		
		long last = System.nanoTime();
		long now = 0;
		while(_running)
		{	
//			try {
//				sleep(_STEP);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
			_manager.lock();
			
			try{
				if(!_paused){
					now = System.nanoTime();
					_manager.Update(now - last);
					last = now;
				}
			}
			finally{
				_manager.unlock();
				
			}
			
		}
		
		_manager.cleanup();
		_manager = null;
	}
}
