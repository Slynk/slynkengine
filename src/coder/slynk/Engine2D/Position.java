package coder.slynk.Engine2D;

/**Stores (x,y) coordinates.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public class Position {
	/** Position*/
	public double x, y;
	
	/**Defaults to x=0, y=0.*/
	public Position()
	{
		x = 0.0f;
		y = 0.0f;
	}
	/**Instantiates Position with given position.*/
	public Position(double X, double Y)
	{
		x = X;
		y = Y;
	}
}
