package coder.slynk.Engine2D.Glue;

public class Time {
	public static final int SECONDS = 1000000000;
	public static final int MILISECONDS = 1000000;
}
