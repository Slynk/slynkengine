package coder.slynk.Engine2D;

/**A typical two variable class holding height and width.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public class Proportions {
	/**Stores size.*/
	public int width, height;
	
	/**Instantiates size with height = 0 and width = 0.*/
	public Proportions()
	{
		width = 0;
		height = 0;
	}
	/**Instantiates size with specified dimensions.
	 * 
	 * @param w sets width.
	 * @param h sets height.
	 */
	public Proportions(int w, int h)
	{
		width = w;
		height = h;
	}
}
