package coder.slynk.Engine2D.Input;

import javax.microedition.khronos.opengles.GL10;

import coder.slynk.Engine2D.Util;
import coder.slynk.Engine2D.Graphics.Sprite;

/** Defines a button. Stores a sprite and can return whether it's pressed or not.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public class Button {
	private Sprite _image;
	
	/** Instantiates Button with a given sprite.
	 * 
	 * @param image the sprite associated with this button.
	 */
	public Button(Sprite image){
		_image = image;
	}
	
	/**Draws the button.
	 * 
	 * @param gl GL10 reference.
	 */
	public void draw(GL10 gl){
		_image.draw(gl);
	}
	
	/**Returns whether the button was touched given position.
	 * 
	 * @param x the x position touched.
	 * @param y the y position touched.
	 * @return True if touched or false if not touched.
	 */
	public boolean touched(double x, double y)
	{
		if(		(Util.checkRange(_image.x(), _image.x() + _image.width(), x) == 0) && 
				(Util.checkRange(_image.y(), _image.y() + _image.height(), y) == 0))
			return true;
		else
			return false;
	}
}
