package coder.slynk.Engine2D;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import coder.slynk.Engine2D.States.StateManager;

import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.util.Log;

/**GLRenderer locks and calls the state manager's draw function.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public class GLRenderer implements GLSurfaceView.Renderer {
	private StateManager _manager;
	
	/**Instantiate GLRenderer
	 * 
	 * @param m reference to the state manager.
	 */
	public GLRenderer(StateManager m){
		_manager = m;
	}

	/**Locks and calls Draw(gl) from the state manager.
	 * @param gl GL10 reference.
	 */
	public void onDrawFrame(GL10 gl) {
	    _manager.lock();
    	try{
    		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
    		_manager.Draw(gl);
    	}
    	finally{
    		_manager.unlock();
    	}
	}
	
	/** Sets up an Ortho2D projection.
	 * (0,0) is the top left corner and (width, height) is the bottom right corner.
	 * 
	 * @param gl GL10 reference.
	 * @param width the screen width.
	 * @param height the screen height.
	 */
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		DeviceInformation.setWidth(width);
		DeviceInformation.setHeight(height);
		gl.glViewport(DeviceInformation.getX(), DeviceInformation.getY(), width, height);
		
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glLoadIdentity();
		GLU.gluOrtho2D(gl, 0, width, height, 0);
		
		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glLoadIdentity();
	}
	
	/**Sets up OpenGL for 2D drawing and calls reload() from the state manager.*/
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		
		gl.glClearColor(0.5f, 0.5f, 1.0f, 1.0f);
		gl.glShadeModel(GL10.GL_SMOOTH);
		
		gl.glDisable(GL10.GL_DEPTH_BUFFER_BIT);
		gl.glDisable(GL10.GL_DEPTH_TEST);
		gl.glDisable(GL10.GL_CULL_FACE);
		gl.glDisable(GL10.GL_DITHER);
        gl.glDisable(GL10.GL_LIGHTING);
        
        gl.glEnable(GL10.GL_TEXTURE_2D);
        
        gl.glTexEnvx(GL10.GL_TEXTURE_ENV, GL10.GL_TEXTURE_ENV_MODE, GL10.GL_MODULATE);
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
        																							Log.v("SlynkEngine", "GL Surface Created!");
        _manager.lock();
        try{
        	_manager.reload();
        }
        finally{
        	_manager.unlock();                                                                      Log.v("SlynkEngine", "State Manager Reload Complete!");
        }
	}

}
