package coder.slynk.Engine2D.Text;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.LinkedList;
import java.util.Queue;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import coder.slynk.Engine2D.Proportions;

/**A self contained area to type strings with a given font.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public class TextArea {
	private Font _font; // To house the glyphs
	private String _text = null; //Text being drawn 
	private final Queue<String> _queuedText = new LinkedList<String>(); // Next Text to be Drawn
	private float _x, _y, _dx, _dy;
	private final Proportions _area;
	private FloatBuffer _vertexBuffer, _textureBuffer;
	private ShortBuffer _indexBuffer;
	private int _textureID;
	private int _vertexID;
	private int _indexID;
	private int _verticesCount;
	private int _charCount;
	private boolean _dirty = true;
	private boolean _empty = true;

	/**Instantiates TextArea
	 * 
	 * @param X the X position of the top left corner of the area.
	 * @param Y the Y position of the top left corner of the area.
	 * @param dim the size of the area.
	 * @param f the font to draw with.
	 */
	public TextArea(float X, float Y, Proportions dim, Font f) {
		_font = f;
		_x=_dx=X;
		_y=_dy=Y;
		_area = dim;
	}
	
	/**Add text to the queue.
	 * 
	 * @param s a queue of strings to draw in order.
	 */
	public final void addText(Queue<String> s)
	{
		_queuedText.addAll(s);
		if(_empty)
		{
			_text = new String();
			_text = _queuedText.remove();
			if(!_queuedText.isEmpty())
				_empty = false;
			_fillBuffers();
		}
	}
	
	/**Add text to the queue.
	 * 
	 * @param s an array of strings to draw in order.
	 */
	public final void addText(String[] s)
	{
		for(int i = 0; i < s.length; i++)
			_queuedText.add(s[i]);
		
		if(_empty)
		{
			_text = new String();
			_text = _queuedText.remove();
			if(!_queuedText.isEmpty())
				_empty = false;
			_fillBuffers();
		}
	}
	
	/**Advance text to the next string.*/
	public final void advanceText()
	{
		if(!_empty)
		{
			_text = _queuedText.remove();
			if(_queuedText.isEmpty())
				_empty = true;
			_dirty = true;
			_dx = _x;
			_dy = _y;
			_fillBuffers();
		}
	}
	
	protected final void _fillBuffers()
	{
		
		char[] chars = _text.toCharArray();
		_charCount = 0;
		for(char c : chars)
		{			
			switch(c)
			{
			case ' ':
				break;
			case '\n':
				break;
			default:
				_charCount++;
				break;
			}
		}
		
		ByteBuffer bb = ByteBuffer.allocateDirect(_charCount*12 * 4);
		bb.order(ByteOrder.nativeOrder());
		_vertexBuffer = bb.asFloatBuffer();
		
		ByteBuffer bb2 = ByteBuffer.allocateDirect(_charCount*8 * 4);
		bb2.order(ByteOrder.nativeOrder());
		_textureBuffer = bb2.asFloatBuffer();
		
		float[] array = new float[]{};

		int len = chars.length;
		for(int i = 0; i < len; i++)
		{
			char c = chars[i];
			
			switch(c)
			{
			case ' ':
				if(_isTooBig(array))
				{
					_dx = _x;
					_dy += _font.getFontSize()+10;
				}
				_vertexBuffer.put(_moveGlyph(array));
				_textureBuffer.put(_extractTextureCoords(array));

				array = new float[]{};
				_dx += _font.getFontSize()/3;
				break;
			case '\n':
				if(_isTooBig(array))
				{
					_dx = _x;
					_dy += _font.getFontSize()+10;
				}
				_vertexBuffer.put(_moveGlyph(array));
				_textureBuffer.put(_extractTextureCoords(array));
				
				array = new float[]{};
				_dx = _x;
				_dy += _font.getFontSize()+10;
				break;
			default:
				float[] temp = _font.getChar(c);
				float[] t = new float[temp.length+array.length];
				System.arraycopy(array, 0, t, 0, array.length);
				System.arraycopy(temp, 0, t, array.length, temp.length);
				array = t;
				
				if(i==(len-1)) 
				{
					if(_isTooBig(array))
					{
						_dx = _x;
						_dy += _font.getFontSize()+10;
					}
					_vertexBuffer.put(_moveGlyph(array));
					_textureBuffer.put(_extractTextureCoords(array));

					array = new float[]{};
					_dx += _font.getFontSize()/3;
				}
				
				break;
			}
			
			
			
		}
		
		ByteBuffer ibb = ByteBuffer.allocateDirect(_charCount * 6 * 2);
        ibb.order(ByteOrder.nativeOrder());
        _indexBuffer = ibb.asShortBuffer();
        
        for(short i = 0; i < _charCount; i++)
        {
        	short t = (short) (i*4);
        	_indexBuffer.put(t);
        	_indexBuffer.put((short) (t+1));
        	_indexBuffer.put((short) (t+2));
        	
        	_indexBuffer.put((short) (t+1));
        	_indexBuffer.put((short) (t+2));
        	_indexBuffer.put((short) (t+3));
        }  
        
        _indexBuffer.position(0);
        _textureBuffer.position(0);
        _vertexBuffer.position(0);
        _dx = _x;
        _dy = _y;
	}
	
	protected final boolean _isTooBig(float[] array)
	{
		float size = 0;
		
		for(int i = 0; i < (array.length/20); i++)
		{
			size += (array[(i*20) + 6] - array[(i*20)]);
		}
		
		if((_dx + size) > (_area.width + _x))
			return true;
		else
			return false;
	}
	
	protected final float[] _extractTextureCoords(float[] old)
	{
		float[] temp = new float[(old.length/20)*8];
		for(int i = 0; i < (old.length/20); i++)
		{
			temp[(i*8)+0] = old[(i*20)+12];
			temp[(i*8)+1] = old[(i*20)+13];
			temp[(i*8)+2] = old[(i*20)+14];
			temp[(i*8)+3] = old[(i*20)+15];
			temp[(i*8)+4] = old[(i*20)+16];
			temp[(i*8)+5] = old[(i*20)+17];
			temp[(i*8)+6] = old[(i*20)+18];
			temp[(i*8)+7] = old[(i*20)+19];
		}
		
		return temp;
		
	}
	
	protected final float[] _moveGlyph(float[] old)
	{
		float[] moved = new float[(old.length/20)*12];
		for(int i = 0; i < (old.length/20); i++)
		{
			moved[(i*12)] = old[(i*20)+0]+_dx;
			moved[(i*12)+1] = old[(i*20)+1]+_dy;
			moved[(i*12)+2] = 0;
			
			moved[(i*12)+3] = old[(i*20)+3]+_dx;
			moved[(i*12)+4] = old[(i*20)+4]+_dy;
			moved[(i*12)+5] = 0;
			
			moved[(i*12)+6] = old[(i*20)+6]+_dx;
			moved[(i*12)+7] = old[(i*20)+7]+_dy;
			moved[(i*12)+8] = 0;
			
			moved[(i*12)+9] = old[(i*20)+9]+_dx;
			moved[(i*12)+10] = old[(i*20)+10]+_dy;
			moved[(i*12)+11] = 0;
			
			_dx += old[(i*20)+6] + 3;
		}
		return moved;
	}
	
	public final boolean isEmpty()
	{
		return _empty;
	}
	
	/**Draw the current string.*/
	public void draw(GL10 gl)
	{
		GL11 gl11 = (GL11)gl;
		if(_font.isDirty())
		{
			_fillBuffers();
			_dirty = true;
		}

		if(_dirty)
		{
			int[] buffer = new int[1];
			
			gl11.glGenBuffers(1, buffer, 0);
			_indexID = buffer[0];
			gl11.glBindBuffer(GL11.GL_ELEMENT_ARRAY_BUFFER, _indexID);
			gl11.glBufferData(GL11.GL_ELEMENT_ARRAY_BUFFER, _charCount * 6 * 2, _indexBuffer, GL11.GL_STATIC_DRAW);
			
			gl11.glGenBuffers(1, buffer, 0);
			_vertexID = buffer[0];
			gl11.glBindBuffer(GL11.GL_ARRAY_BUFFER, _vertexID);
			gl11.glBufferData(GL11.GL_ARRAY_BUFFER, _charCount * 4 * 3 * 4, _vertexBuffer, GL11.GL_STATIC_DRAW);
			
			gl11.glGenBuffers(1, buffer, 0);
			_textureID = buffer[0];
			gl11.glBindBuffer(GL11.GL_ARRAY_BUFFER, _textureID);
			gl11.glBufferData(GL11.GL_ARRAY_BUFFER, _charCount * 4 * 2 * 4, _textureBuffer, GL11.GL_STATIC_DRAW);
			
			gl11.glBindBuffer(GL11.GL_ELEMENT_ARRAY_BUFFER, 0);
			gl11.glBindBuffer(GL11.GL_ARRAY_BUFFER, 0);
			
			_verticesCount = _charCount * 6;
			_dirty = false;
		}
		
		gl.glColor4f(_font.R(), _font.G(), _font.B(), _font.A());
		
		gl.glBindTexture(GL10.GL_TEXTURE_2D, _font.getID());
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		
		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		
		
		gl11.glBindBuffer(GL11.GL_ARRAY_BUFFER, _vertexID);
		gl11.glVertexPointer(3, GL11.GL_FLOAT, 0, 0);
		gl11.glBindBuffer(GL11.GL_ARRAY_BUFFER, _textureID);
		gl11.glBindBuffer(GL11.GL_ELEMENT_ARRAY_BUFFER, _indexID);
		gl11.glTexCoordPointer(2, GL11.GL_FLOAT, 0, 0);

		gl11.glDrawElements(GL11.GL_TRIANGLES, _verticesCount, GL11.GL_UNSIGNED_SHORT, 0);
		
		gl11.glBindBuffer(GL11.GL_ELEMENT_ARRAY_BUFFER, 0);
		gl11.glBindBuffer(GL11.GL_ARRAY_BUFFER, 0);
		
		gl.glDisable(GL10.GL_BLEND);
		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		
	}
}
