package coder.slynk.Engine2D.Text;


import java.util.HashMap;
import java.util.Vector;
import coder.slynk.Engine2D.Graphics.Color4f;
import coder.slynk.Engine2D.Graphics.Image;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.util.Log;

/**Converts a bitmap into a workable font.
 * 
 * This relies on a bitmap that has at least one row of fully transparent pixels between char rows in the file,
 * at least one column of fully transparent pixels between chars within a row (but not necessarily the full file),
 * and that disconnected chars, such as the double quote, be connected by a partially opaque area of pixels. (1 alpha
 * is fine, as long as it's not 0 alpha.)
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public final class Font{
	/**Associates a char with its texture coordinates.*/ 	private HashMap<Character, float[]> _glyphs = new HashMap<Character, float[]>();
	/**Tweak font coordinates.*/							private HashMap<Character, float[]> _tweaks = new HashMap<Character, float[]>();
	/**The font's image.*/									private Image _image;
	/**char array in readable order from the bitmap.*/ 		private char[] _c;
	/**Font Color.*/										private Color4f _color = new Color4f(1f,1f,1f,1f);
	/**Font Size.*/											private int _fontSize = 50;
	/**Tell whether textArea needs updating.*/				private boolean _dirty = false;
	
	/**Creates a font.
	 * 
	 * @param c the char array in readable order from the bitmap.
	 * @param img the image associated with the font.
	 * @param main a reference to the Activity.
	 */
	public Font(char[] c, Image img, Activity main) {
		_c = c;
		_image = img;

		//-------------------------------------------------------------------------------------------------------------------------
		Vector<Integer> t = new Vector<Integer>(), b = new Vector<Integer>(), l = new Vector<Integer>(), r = new Vector<Integer>();
		boolean findingTop= true, findingLeft = true;
		
		Matrix flip = new Matrix();
		flip.postScale(1f, -1f);
		
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inScaled = false;
		
		Bitmap tmp = BitmapFactory.decodeResource( main.getResources(), _image.retrieveResourceID(), opts);
		Bitmap bmp = Bitmap.createBitmap(tmp, 0, 0, tmp.getWidth(), tmp.getHeight(), flip, true);
		tmp.recycle();
		
		int iMin = 0, iMax = 0, xMin = 0, xMax = _image.fullWidth(), yMin = _image.fullHeight(), yMax = 0;
		
		for(int y = yMin - 1; y >= yMax; y--)
			for(int x = xMin; x < xMax; x++)
			{
				
				int pixel = bmp.getPixel(x, y);
				if(findingTop && (Color.alpha(pixel) != 0))
				{
					b.add(y);
					findingTop = false;
					x = xMax;
				}
				else if(!findingTop && (Color.alpha(pixel) != 0))
				{
					x = xMax;
				}
				else if(!findingTop && (Color.alpha(pixel) == 0) && (x == xMax - 1))
				{
					t.add(y);
					findingTop = true;
				}
			}
		
		iMax = t.size();
		
		for(int i = iMin; i < iMax; i++)
			for(int x = xMin; x < xMax; x++)
			{
				yMin = t.elementAt(i); yMax = b.elementAt(i);
				for(int y = yMin; y < yMax; y++)
				{
					int pixel = bmp.getPixel(x, y);
					if(findingLeft && (Color.alpha(pixel) != 0))
					{
						l.add(x);
						findingLeft = false;
						y = b.elementAt(i);
					}
					else if(!findingLeft && (Color.alpha(pixel) != 0))
					{
						y = b.elementAt(i);
					}
					else if(!findingLeft && (Color.alpha(pixel) == 0) && (y == b.elementAt(i) - 1))
					{
						r.add(x);
						findingLeft = true;
					}		
				}
			}
		bmp.recycle();
		
		xMax = _image.fullWidth();
		yMin = _image.fullHeight();
		int count = l.size();
		int currentRow = 0;
		int lastX = 0;
		if(count != _c.length)
		{
			Log.v("SlynkEngine", "Font Initialization warning. Character array length does not match extracted glyph count.");
			Log.v("SlynkEngine", "Extracted: " + count + " Char count: " + _c.length);
			if(_c.length < count)
				count = _c.length;
		}
		
		for(int i = 0; i < count; i++)
		{
			int currentX = l.elementAt(i);
			
			if(currentX < lastX)
			{
				currentRow++;
			}
			float temp[] = new float[8];			
			temp[0] = ((float)l.elementAt(i))/xMax; // x of Bottom Left
			temp[1] = ((float)b.elementAt(currentRow))/yMin ; // y of Bottom Left
			
			temp[2] = ((float)l.elementAt(i))/xMax; // x of Top Left
			temp[3] = ((float)t.elementAt(currentRow))/yMin; // y of Top Left
			
			temp[4] = ((float)r.elementAt(i))/xMax; // x of Bottom Right
			temp[5] = ((float)b.elementAt(currentRow))/yMin; // y of Bottom Right
			
			temp[6] = ((float)r.elementAt(i))/xMax; // x of Top Right
			temp[7] = ((float)t.elementAt(currentRow))/yMin; // y of Top Right
			
			_glyphs.put(_c[i], temp);
			
			lastX = currentX;
		}
	}
	
	
	/**Retrieves an array containing vertices and texture coordinates for a character.
	 * 
	 * @param a the char to retrieve.
	 * @return First 12 indexes are the vertices of the square at (0,0) with the correct size.
	 * The last 8 house the texture coordinates for the string.
	 */
	public float[] getChar(char a)
	{
		float temp[] = _glyphs.get(a);
		float x = 0, y = 0;
		
		float ratio = ((float)_fontSize)/((temp[1] - temp[3])*_image.fullHeight());
		if(_tweaks.containsKey(a))
		{
			float t[] = _tweaks.get(a);
			x = t[0]*ratio;
			y = t[1]*ratio;
		}
		float ret[] = new float[]{
				x, y, 0,    															// Top Left
				x, _fontSize+y, 0, 														// Bottom Left
				((temp[4] - temp[2])*_image.fullWidth()*ratio) + x, y, 0, 							// Top Right
				((temp[4] - temp[2])*_image.fullWidth()*ratio) + x, _fontSize + y, 0, 					// Bottom Right
				temp[0], temp[1], temp[2], temp[3], temp[4], temp[5], temp[6], temp[7]  // Texture array
			};
		return ret;
	}
	
	/**Return texture ID*/
	public int getID()
	{
		return _image.retrieveID();
	}
	
	/**Sets a new alpha for the square.
	 * 
	 * @param opacity new alpha.
	 */
	public void fade(float opacity)
	{
		_color.setColor(_color.r, _color.g, _color.b, opacity);
	}
	
	/**Sets a new color for the square.
	 * 
	 * @param c new color.
	 */
	public void setColor(Color4f c)
	{
		_color = c;
	}
	
	/**Sets a new color for the square.
	 * 
	 * @param R Red.
	 * @param G Green.
	 * @param B Blue.
	 * @param A Alpha.
	 */
	public void setColor(float R, float G, float B, float A)
	{
		_color = new Color4f(R, G, B, A);
	}
	/**Returns the red value.*/
	public float R(){return _color.r;}
	/**Returns the green value.*/
	public float G(){return _color.g;}
	/**Returns the blue value.*/
	public float B(){return _color.b;}
	/**Returns the alpha value.*/
	public float A(){return _color.a;}
	
	/**Sets the font size.
	 * 
	 * @param fSize new font size.
	 */
	public void setFontSize(int fSize)
	{
		_fontSize = fSize;
		_dirty = true;
	}

	/**Used to update buffers if font size changes.*/
	public boolean isDirty() {
		if(_dirty)
		{
			_dirty = false;
			return true;
		}
		else
			return false;
	}

	/**Use this to tweak individual charater's positions.
	 * 
	 * @param t hash map of tweaks.
	 * */
	public void tweakFont(HashMap<Character, float[]> t)
	{
		_tweaks = t;	
	}
	
	/**Returns font size
	 * 
	 * @return _fontSize
	 */
	public int getFontSize()
	{
		return _fontSize;
	}
}
