package coder.slynk.Engine2D;

/**Holds information on the screen size and position.
 * 
 * @author Slynk
 * @version v1.0 May 5, 2012
 */
public final class DeviceInformation {
	private static int _screenWidth, _screenHeight, _screenX = 0, _screenY = 0;
	
	/**Returns the screen width.*/
	public final static int getWidth()
	{return _screenWidth;}
	
	/**Sets the screen width.*/
	public final static void setWidth(int t)
	{ _screenWidth = t;}
	
	/**Returns the screen height.*/
	public final static int getHeight()
	{return _screenHeight;}
	
	/**Sets the screen height.*/
	public final static void setHeight(int t)
	{ _screenHeight = t;}
	
	/**Returns the screen x position.*/
	public final static int getX()
	{return _screenX;}
	
	/**Sets the screen x position.*/
	public final static void setX(int t)
	{ _screenX = t;}
	
	/**Returns the screen y position.*/
	public final static int getY()
	{return _screenY;}
	
	/**Sets the screen y position.*/
	public final static void setY(int t)
	{ _screenY = t;}
}
