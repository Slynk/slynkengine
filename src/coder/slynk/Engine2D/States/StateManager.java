package coder.slynk.Engine2D.States;

import java.util.Vector;
import java.util.concurrent.locks.ReentrantLock;

import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.util.Log;

import coder.slynk.Engine2D.GLView;
import coder.slynk.Engine2D.Process.ProcessManager;

/**Manages the states vector.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public class StateManager {
	private final ProcessManager m_ProcessManager = new ProcessManager();
	private GLView _glv;
	private Activity _context;
	private Vector<State> _states;
	private ReentrantLock  _lock;
	private int _lastState = 0;
	private boolean _running = false;
	private boolean _initialLoad = true;
	
	/**Instantiates a state manager.
	 * 
	 * @param s the initial state to push onto the vector.
	 * @param main a reference to the activity.
	 */
	public StateManager(State s, Activity main){
		_context = main;
		_glv = new GLView(main, this);
		main.setContentView(_glv);
		_states = new Vector<State>(3);
		_states.add(s);
		_lock = new ReentrantLock(false);
	}
	
	/**Push a state onto the end of the vector.
	 * 
	 * @param s the state to push.
	 */
	public void pushState(State s){
		s._loadResources(_glv, _context);
		_states.elementAt(_lastState)._pause();
		_states.add(s);
		_lastState++;
	}
	
	/**Remove current state.*/
	public void popState(){
		_states.elementAt(_lastState)._callCleanup();
		_states.remove(_lastState);
		_lastState--;

		Log.v("SlynkEngine", "lastState: " + _lastState);
		if(_lastState == -1)
		{
			_running = false;
			_context.finish();
		}
		else
			_states.elementAt(_lastState)._resume();
	}
	
	/**Remove current state and push new state.
	 * 
	 * @param s new state.
	 */
	public void swapState(State s){
		_states.elementAt(_lastState)._callCleanup();
		s._loadResources(_glv, _context);
		_states.setElementAt(s, _lastState);
	}
	
	/**Forward input event to current state.*/
	public void Input(double x, double y){
		if(_running)
			_states.elementAt(_lastState)._callInput(x, y);
	}
	/**Call current state's _Update() method.*/
	public void Update(final long deltaNs){
		if(_running)
		{
			_states.elementAt(_lastState)._callUpdate(deltaNs);
			if(_states.elementAt(_lastState)._shouldChangeState())
				_states.elementAt(_lastState)._changeState(this);
		}
	}
	/**Call current state's _Draw() method.*/
	public void Draw(GL10 gl){
		if(_running)
			_states.elementAt(_lastState)._callDraw(gl);
	}
	
	/**Reloads resources for the full state vector upon resume.*/
	public void reload(){
		_running = true;
		
		if(_initialLoad)
		{
			for(State s : _states)
				s._loadResources(_glv, _context);
			
			_initialLoad = false;
		}
		else
		{
			for(State s : _states)
				s._reloadImages();
		}
		
	}

	/**Pauses the current state.*/
	public void pause() {
			_glv.onPause(); 
			if(_lastState != -1)
			{
				_states.elementAt(_lastState)._pause();
			}
	}
	/**Resumes the current state.*/
	public void resume(){
			_glv.onResume(); 
			if(_lastState != -1)
			{
				_states.elementAt(_lastState)._callResume();
			}
		}
	
	/**Lock the state manager for a thread.*/
	public void lock(){_lock.lock();}
	/**Unlock the state manager for a thread.*/
	public void unlock(){_lock.unlock();}
	
	/**Cleans up the state vector.*/
	public void cleanup()
	{
		while(_lastState > -1)
		{
			_states.elementAt(_lastState)._callCleanup();
			popState();
		}
	}
	
	public ProcessManager processManager(){
		return m_ProcessManager;
	}
}
