package coder.slynk.Engine2D.States;

import java.util.Vector;

import javax.microedition.khronos.opengles.GL10;

import coder.slynk.Engine2D.Input.Button;

/**An extension of State to hadle menus.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public abstract class Menu extends State {
	
	/**The index of the last button pressed.*/
	protected short ButtonPressed = -1;
	private final Vector<Button> buttons = new Vector<Button>();

	/**Cycles through the buttons testing whether any were pressed.
	 * 
	 * @param x the x position of the touch event.
	 * @param y the y position of the touch event.
	 */
	@Override
	protected void _input(double x, double y) {
		
		int size = buttons.size();
		
		for(short i = 0; i < size; i++)
		{
			if(buttons.elementAt(i).touched(x, y))
			{
				ButtonPressed = i;
			}
		}
	}

	/**Checks whether a ButtonPressed has changed.
	 * If so, call ChangeState().*/
	@Override
	public void _update(final long deltaNs) {
		if (ButtonPressed != -1)
			_shouldChangeState = true;

	}

	/**Draw under the buttons. 
	 * 
	 * @param gl GL10 reference.
	 */
	protected abstract void _drawBefore(GL10 gl);
	@Override
	public final void _draw(GL10 gl) {
		_drawBefore(gl);
		
		for(Button b : buttons)
			b.draw(gl);
		
		_drawAfter(gl);

	}
	
	/**Draw over the buttons.
	 * 
	 * @param gl GL10 reference.
	 */
	protected abstract void _drawAfter(GL10 gl);
	
	/**Add the button to the menu's draw list.
	 * 
	 * @param b button to be added.
	 */
	protected final void _addButton(Button b)
	{
		buttons.add(b);
	}
	
	/**Returns which button has been pressed.
	 * 
	 * @return ButtonPressed.
	 */
	protected final short _whichIsPressed()
	{
		return ButtonPressed;
	}

}