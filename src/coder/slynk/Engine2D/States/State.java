package coder.slynk.Engine2D.States;

import java.util.Vector;

import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;

import coder.slynk.Engine2D.GLView;
import coder.slynk.Engine2D.Graphics.Image;
import coder.slynk.Engine2D.Graphics.ImageCrop;

/**Abstract base class for any game state.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public abstract class State {
	private boolean _texturesAreLoaded = false;
	private boolean _resourcesAreLoaded = false;
	
	/**Vector of Images*/						  						protected Vector<Image> _images = new Vector<Image>();
	/**Reference to the GLView*/                  						protected GLView _glv;
	/**The activity's reference.*/                						protected Activity _context;
	
	/**Variable to change so that StateManager will call ChangState()*/ protected boolean _shouldChangeState = false;
	/**Number to change to designate which state to change to.*/		protected int _switchStateNum = -1;
	
	
	/**Called by StateManager upon creation.
	 * 
	 * Tells the GLView thread to call State.LoadTextures().
	 * 
	 * @param glv the GLView reference.
	 * @param context the Activity reference.
	 */
	void _loadResources(GLView glv, Activity context)
	{
		_glv = glv;
		_context = context;
		
		glv.queueEvent(new Runnable() {
            public void run() {
            	_textures();
            	_texturesAreLoaded = true;
            }
        });
		
		if(!_resourcesAreLoaded)
		{
			_resources();
			_resourcesAreLoaded = true;
		}
	}
	
	/**Override and provide logic to switch state.*/ 		
	protected abstract void _changeState(StateManager manager);
	
	/**Returns whether the state is requesting a change.
	 * 
	 * @return {@link _shouldChangeState} 
	 */	
	boolean _shouldChangeState(){return _shouldChangeState;}
	
	/**Forwards touch event to Input() if state is loaded.
	 * 
	 * @param x the x position of the touch event.
	 * @param y the y position of the touch event.
	 */
	void _callInput(double x, double y)
	{
		if(_texturesAreLoaded && _resourcesAreLoaded)
			_input(x, y);
	}
	
	/**Calls Update() is state is loaded*/
	void _callUpdate(final long deltaNs)
	{
		if(_texturesAreLoaded && _resourcesAreLoaded)
			_update(deltaNs);
	}
	
	/**Calls Draw() if state is loaded.*/
	void _callDraw(GL10 gl)
	{
		if(_texturesAreLoaded)
			_draw(gl);
	}
	
	/**Forwards resource ID to GLView's loadTexture() function
	 * 
	 * @param resourceID the texture's resource ID.
	 * @return the finalized Image object.
	 */
	protected Image _loadTexture(int resourceID)
	{
		_images.add(_glv.loadTexture(resourceID));
		return _images.lastElement();
	}
	
	/**Forwards resource ID to GLView's loadTexture() function
	 * 
	 * @param resourceID the texture's resource ID.
	 * @param c the crop area to set to the image.
	 * @return the finalized Image object.
	 */
	protected Image _loadTexture(int resourceID, ImageCrop c)
	{
		_images.add(_glv.loadTexture(resourceID));
		return _images.lastElement();
	}
	
	/**Sets the new texture ID to all images upon reload.*/
	void _reloadImages()
	{
		_glv.queueEvent(new Runnable() {
            public void run() {
            	for(Image i: _images)
        			_glv.reloadImage(i);
            }
        });
	}
	
	/**Override to initialize anything other than textures.*/	
	protected abstract void _resources();
	/**Override to initialize textures.*/						
	protected abstract void _textures();
	
	/**Tells GLView to delete the textures.*/
	void _destroyTextures()
	{
		final int[] _textures = new int[_images.size()];
		for(int i = 0; i < _images.size(); i++)
		{
			_textures[0] = _images.elementAt(i).retrieveID();
		}
		
		_glv.queueEvent(new Runnable() {
            public void run() {
            	_glv.deleteTextures(_textures);
            }
        });
	}
	
	/**Override to handle input.*/
	protected abstract void _input(double x, double y);
	/**Override to Update logic.*/
	protected abstract void _update(final long deltaNs);
	/**Override to Draw objects.*/
	protected abstract void _draw(GL10 gl);
	
	/**Calls Resume()*/
	void _callResume()
	{
		if(_resourcesAreLoaded)
			_resume();
	}
	
	/**Calls DestroyTextures and Cleanup()*/
	void _callCleanup()
	{
		_destroyTextures();
		_cleanup();
	}
	
	/**Override with your own logic to run upon minimizing the activity.*/
	protected abstract void _pause();
	/**Override with your own logic to run upon maximizing the activity.*/
	protected abstract void _resume();
	/**Override with your own cleanup code to run upon closing the state.*/
	protected abstract void _cleanup();
}
