package coder.slynk.Engine2D;



import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.opengles.GL10;

import coder.slynk.Engine2D.Graphics.Image;
import coder.slynk.Engine2D.Graphics.ImageCrop;
import coder.slynk.Engine2D.States.StateManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.util.Log;
import android.view.MotionEvent;

/**Extends GLSurfaceView for use with the engine.
 * 
 * @author Slynk
 * @version v1.0 Jan 1, 2012
 */
public class GLView extends GLSurfaceView  {
	private StateManager _manager;
	private GLRenderer _renderer;
	private Context _context;
	
	private static int _newTextureID(GL10 gl){
		int[] temp = new int[1];
		gl.glGenTextures(1, temp, 0);
		Log.v("IntoLin", "ID Generated: " + temp[0]);
		return temp[0];
	}

	/** Instantiates GLView.
	 * 
	 * @param context the context from the application's activity.
	 * @param m reference to the state manager.
	 */
	public GLView(Context context, StateManager m) {
		super(context);
		_context = context;
		_manager = m;
		_renderer = new GLRenderer(_manager);
		setRenderer(_renderer);
		Log.v("IntoLin", "Renderer Created!");
	}
	
	/**Called anytime the screen is touched.
	 * Sends (X,Y) touch coordinate to state manager.*/
	public boolean onTouchEvent(final MotionEvent event){
		_manager.lock();
        _manager.Input(event.getX(), event.getY());
        _manager.unlock();
		return true;
	}	
	
	/**Called when application is resumed. OpenGL needs to reload the images.
	 * 
	 * @param img image to reload.
	 */
	public void reloadImage(Image img)
	{
		final Image tmpImage = loadTexture(img.retrieveResourceID());
		
		img.setNewTextureID(tmpImage.retrieveID());
	}
	
	/**Generates a texture given a resource ID.
	 * 
	 * @param ResourceID the resource ID for the given image.
	 * @return Texture ID for later binding.
	 */
	public Image loadTexture(int ResourceID){
				EGL10 egl = (EGL10)EGLContext.getEGL(); 
				GL10 gl = (GL10)egl.eglGetCurrentContext().getGL();
				
				int id = _newTextureID(gl);
				Proportions prop;
				
				Matrix flip = new Matrix();
				flip.postScale(1f, -1f);
				
				BitmapFactory.Options opts = new BitmapFactory.Options();
				opts.inScaled = false;
				
				Bitmap temp = BitmapFactory.decodeResource(_context.getResources(), ResourceID, opts);
				Bitmap bmp = Bitmap.createBitmap(temp, 0, 0, temp.getWidth(), temp.getHeight(), flip, true);
				temp.recycle();
				
				prop = new Proportions(bmp.getWidth(), bmp.getHeight());
				
				gl.glBindTexture(GL10.GL_TEXTURE_2D, id);
				
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR_MIPMAP_NEAREST);
			    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR_MIPMAP_NEAREST);
			    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT);
			    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT);
				
				for(int level = 0, height = bmp.getHeight(), width = bmp.getWidth(); true; level++){
					GLUtils.texImage2D(GL10.GL_TEXTURE_2D, level, bmp, 0);
					
					if(height == 1 && width==1)	break;
					
					width >>= 1; height >>= 1;
			        if(width<1)  width = 1;
			        if(height<1) height = 1;
			        
			        Bitmap bmp2 = Bitmap.createScaledBitmap(bmp, width, height, true);
			        bmp.recycle();
			        bmp = bmp2;
				}
				
				bmp.recycle();
				
				return new Image(ResourceID, id, prop);
	}
	/**Generates a texture given a resource ID and stores its size in dim.
	 * 
	 * @param ResourceID the resource ID for the given image.
	 * @param dim used for storing the size of the loaded image.
	 * @return Texture ID for later binding.
	 */
	public Image loadTexture(int ResourceID, ImageCrop crop){
		EGL10 egl = (EGL10)EGLContext.getEGL(); 
		GL10 gl = (GL10)egl.eglGetCurrentContext().getGL();
		
		int id = _newTextureID(gl);
		Proportions prop;
		
		Matrix flip = new Matrix();
		flip.postScale(1f, -1f);
		
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inScaled = false;
		
		Bitmap temp = BitmapFactory.decodeResource(_context.getResources(), ResourceID, opts);
		Bitmap bmp = Bitmap.createBitmap(temp, 0, 0, temp.getWidth(), temp.getHeight(), flip, true);
		temp.recycle();
		
		prop = new Proportions(bmp.getWidth(), bmp.getHeight());
		
		gl.glBindTexture(GL10.GL_TEXTURE_2D, id);
		
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR_MIPMAP_NEAREST);
	    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR_MIPMAP_NEAREST);
	    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT);
	    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT);
		
		for(int level = 0, height = bmp.getHeight(), width = bmp.getWidth(); true; level++){
			GLUtils.texImage2D(GL10.GL_TEXTURE_2D, level, bmp, 0);
			
			if(height == 1 && width==1)	break;
			
			width >>= 1; height >>= 1;
	        if(width<1)  width = 1;
	        if(height<1) height = 1;
	        
	        Bitmap bmp2 = Bitmap.createScaledBitmap(bmp, width, height, true);
	        bmp.recycle();
	        bmp = bmp2;
		}
		
		bmp.recycle();
		
		return new Image(ResourceID, id, prop, crop);
	}

	/**For all IDs in textures[], delete textures.
	 * 
	 * @param textures array of texture IDs.
	 */
	public void deleteTextures(final int[] textures)
	{
		queueEvent(new Runnable(){

			public void run() {
				EGL10 egl = (EGL10)EGLContext.getEGL(); 
				GL10 gl = (GL10)egl.eglGetCurrentContext().getGL();
				
				gl.glDeleteTextures(textures.length, textures, 0);
			}});
	}
}
